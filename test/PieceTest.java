import game.Game;
import game.pieces.*;
import org.junit.Test;

import static game.Game.WHITE;
import static game.Game.BLACK;
import static org.junit.Assert.*;


public class PieceTest {

    public static final int[] ii = new int[]{5,5,5,4,4,7,7,4,4,4,2,3,5,6};
    public static final int[] jj = new int[]{4,5,6,5,6,4,7,1,2,4,4,4,3,4};

    public void testPiece(Piece piece, int[] arr) {
        assertEquals(arr.length, ii.length);
        for(int idx = 0 ; idx < arr.length ; idx ++) {
            testPieceSingle(piece, arr[idx], ii[idx], jj[idx]);
        }
    }

    public void testPieceSingle(Piece piece, int expect, int newI, int newJ) {
        Game game = new Game();
        Piece[][] board = game.getBoard();
        board[4][4] = piece;
        board[5][3] = new Pawn(BLACK);
        board[3][4] = new Pawn(BLACK);
        board[4][2] = new Pawn(WHITE);
        for(int i=0 ; i<Game.SIZE ; i++) {
            for(int j=0 ; j<Game.SIZE ; j++) {
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }
        if(expect == 1) {
            assertTrue(piece.getClass().getName()+" should move to "+newI+" "+newJ,piece.move(board, newI, newJ));
        }else {
            assertFalse(piece.getClass().getName()+" should not move to "+newI+" "+newJ,piece.move(board, newI, newJ));
        }
    }

    @Test
    public void testKing() {
        int[] arr = new int[]{1,1,0,1,0,0,0,0,0,0,0,1,1,0};
        testPiece(new King(WHITE), arr);
    }

    @Test
    public void testQueen() {
        int[] arr = new int[]{1,1,0,1,1,1,1,0,0,0,0,1,1,1};
        testPiece(new Queen(WHITE), arr);
    }

    @Test
    public void testBishop() {
        int[] arr = new int[]{0,1,0,0,0,0,1,0,0,0,0,0,1,0};
        testPiece(new Bishop(WHITE), arr);
    }

    @Test
    public void testPawn() {
        int[] arr = new int[]{1,0,0,0,0,0,0,0,0,0,0,0,1,0};
        testPiece(new Pawn(WHITE), arr);
    }

    @Test
    public void testRook() {
        int[] arr = new int[]{1,0,0,1,1,1,0,0,0,0,0,1,0,1};
        testPiece(new Rook(WHITE), arr);
    }

    @Test
    public void testKnight() {
        int[] arr = new int[]{0,0,1,0,0,0,0,0,0,0,0,0,0,0};
        testPiece(new Knight(WHITE), arr);
    }

    @Test
    public void testPawn2() {
        int[] arr = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        testPiece(new Pawn(BLACK), arr);
    }

}
