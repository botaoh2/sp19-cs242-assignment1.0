import game.Game;
import game.pieces.*;
import org.junit.Test;

import static game.Game.WHITE;
import static game.Game.BLACK;
import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void testCheck() {
        Game game = new Game();
        Piece[][] board = game.getBoard();
        board[0][3] = new King(WHITE);
        board[0][0] = new Rook(WHITE);
        board[7][5] = new King(BLACK);
        for(int i=0 ; i<Game.SIZE ; i++) {
            for(int j=0 ; j<Game.SIZE ; j++) {
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }
        assertFalse(game.isCheck());
        assertFalse(game.isCheck());
        game.takeTurn(WHITE,0,0,7,0);
        assertTrue(game.isCheck());
        assertFalse(game.isCheckMate());
    }

    @Test
    public void testCheckmate() {
        Game game = new Game();
        Piece[][] board = game.getBoard();
        board[0][3] = new King(WHITE);
        board[0][0] = new Rook(WHITE);
        board[6][1] = new Rook(WHITE);
        board[7][5] = new King(BLACK);
        for(int i=0 ; i<Game.SIZE ; i++) {
            for(int j=0 ; j<Game.SIZE ; j++) {
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }
        assertFalse(game.isCheckMate());
        assertFalse(game.isCheckMate());
        game.takeTurn(WHITE,0,0,7,0);
        assertTrue(game.isCheckMate());
    }

    @Test
    public void testStalemate() {
        Game game = new Game();
        Piece[][] board = game.getBoard();
        board[0][3] = new King(WHITE);
        board[1][0] = new Rook(WHITE);
        board[6][1] = new Rook(WHITE);
        board[7][7] = new King(BLACK);
        for(int i=0 ; i<Game.SIZE ; i++) {
            for(int j=0 ; j<Game.SIZE ; j++) {
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }
        assertFalse(game.isStaleMate());
        assertFalse(game.isStaleMate());
        game.takeTurn(WHITE,1,0,1,6);
        assertTrue(game.isStaleMate());
    }

}
