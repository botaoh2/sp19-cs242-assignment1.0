package game.pieces;

import game.Game;

public class Helper {

    public static boolean checkStraight(Piece[][] board, int player, int i, int j, int newI, int newJ) {
        if(i == newI && j == newJ) {
            return false;
        }
        if(i != newI && j != newJ) {
            return false;
        }
        int di, dj;
        if(newI == i && newJ > j) {//right
            di = 0;
            dj = 1;
        }else if(newI == i && newJ < j) {//left
            di = 0;
            dj = -1;
        }else if(newJ == j && newI > i) {//up
            di = 1;
            dj = 0;
        }else{//down
            di = -1;
            dj = 0;
        }
        return rec(board, player, i+di, j+dj, newI, newJ, di, dj);
    }

    public static boolean checkDiagonal(Piece[][] board, int player, int i, int j, int newI, int newJ) {
        if(i == newI && j == newJ) {
            return false;
        }
        if(Math.abs(newI-i) != Math.abs(newJ-j)) {
            return false;
        }
        int di, dj;
        if(newI > i && newJ > j) { //up right
            di = 1;
            dj = 1;
        }else if(newI > i && newJ < j){ //up left
            di = 1;
            dj = -1;
        }else if(newI < i && newJ > j) { //down right
            di = -1;
            dj = 1;
        }else{ //down left
            di = -1;
            dj = -1;
        }
        return rec(board, player, i+di, j+dj, newI, newJ, di, dj);
    }

    private static boolean rec(Piece[][] board, int player, int i, int j, int newI, int newJ, int di, int dj) {
        if(!Game.inBound(i, j)) {
            return false;
        }
        if(board[i][j] != null) {
            Piece piece = board[i][j];
            if(piece.player == player) {
                return false;
            }
            return i == newI && j == newJ;
        }
        return i == newI && j == newJ
                || rec(board, player, i+di, j+dj, newI, newJ, di, dj);
    }


}
