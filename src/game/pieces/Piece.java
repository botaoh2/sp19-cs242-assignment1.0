package game.pieces;

public abstract class Piece {

    protected int i, j;
    protected int player;

    public Piece(int player) {
        this.player = player;
    }

    public abstract char getChar();

    public abstract boolean canMove(Piece[][] board, int newI, int newJ);

    public boolean move(Piece[][] board, int newI, int newJ) {
        if(!canMove(board, newI, newJ)) {
            return false;
        }
        board[i][j] = null;
        board[newI][newJ] = this;
        i = newI;
        j = newJ;
        return true;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getPlayer() {
        return player;
    }

    public String toString() {
        return ""+getChar();
    }

    public void setI(int i) {
        this.i = i;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }
}
