package game.pieces;

import game.Game;

public class Knight extends Piece {

    public Knight(int player) {
        super(player);
    }

    @Override
    public char getChar() {
        if(player == Game.WHITE) {
            return 'N';
        }
        return 'n';
    }

    @Override
    public boolean canMove(Piece[][] board, int newI, int newJ) {
        if(board[newI][newJ] != null && board[newI][newJ].player == this.player) {
            return false;
        }

        int di = Math.abs(newI-i);
        int dj = Math.abs(newJ-j);

        if(!(di == 1 && dj == 2 || di == 2 && dj == 1)) {
            return false;
        }
        return true;
    }

}
