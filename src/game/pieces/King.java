package game.pieces;

import game.Game;

public class King extends Piece {

    public King(int player) {
        super(player);
    }

    @Override
    public char getChar() {
        if(player == Game.WHITE) {
            return 'K';
        }
        return 'k';
    }

    @Override
    public boolean canMove(Piece[][] board, int newI, int newJ) {
        if(board[newI][newJ] != null && board[newI][newJ].player == this.player) {
            return false;
        }
        if(newI == i && newJ == j) {
            return false;
        }
        if(Math.abs(newI-i) > 1 || Math.abs(newJ-j) > 1) {
            return false;
        }
        return true;
    }

}
