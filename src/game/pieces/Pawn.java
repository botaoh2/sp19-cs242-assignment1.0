package game.pieces;

import game.Game;

public class Pawn extends Piece {

    public Pawn(int player) {
        super(player);
    }
    @Override
    public char getChar() {
        if(player == Game.WHITE) {
            return 'P';
        }
        return 'p';
    }

    @Override
    public boolean canMove(Piece[][] board, int newI, int newJ) {
        if(board[newI][newJ] != null && board[newI][newJ].player == this.player) {
            return false;
        }

        //first step
        if(newJ == j) {
            if(player == Game.WHITE && i == 1 && newI == 3 && board[2][j] == null && board[3][j] == null) {
                return true;
            }
            if(player == Game.BLACK && i == 6 && newI == 4 && board[5][j] == null && board[4][j] == null) {
                return true;
            }
        }

        int di = player == Game.WHITE ? 1 : -1;

        if(newI != i+di) {
            return false;
        }

        if(!(board[newI][newJ] != null && Math.abs(newJ-j) == 1 || board[newI][newJ] == null && newJ == j) ) {
            return false;
        }
        return true;
    }

}
