package game.pieces;

import game.Game;

public class Queen extends Piece {

    public Queen(int player) {
        super(player);
    }

    @Override
    public char getChar() {
        if(player == Game.WHITE) {
            return 'Q';
        }
        return 'q';
    }

    @Override
    public boolean canMove(Piece[][] board, int newI, int newJ) {
        if(board[newI][newJ] != null && board[newI][newJ].player == this.player) {
            return false;
        }
        if(newI == i && newJ == j) {
            return false;
        }

        if(!Helper.checkStraight(board, player, i, j, newI, newJ) && !Helper.checkDiagonal(board, player, i, j, newI, newJ)) {
            return false;
        }
        return true;
    }

}
