package game;

import game.pieces.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Game {

    public static final int WHITE = 1;
    public static final int BLACK = 2;
    public static final int SIZE = 8;

    private Piece[][] board;
    private Piece[][] tempBoard;
    private int currentPlayer;
    private int turn;

    public Game() {
        board = new Piece[SIZE][SIZE];
        tempBoard = null;
        turn = 1;
        currentPlayer = WHITE;
    }

    public void layout() {
        //King
        board[0][3] = new King(WHITE);
        board[7][3] = new King(BLACK);
        //Queen
        board[0][4] = new Queen(WHITE);
        board[7][4] = new Queen(BLACK);
        //Rook
        board[0][0] = new Rook(WHITE);
        board[0][7] = new Rook(WHITE);
        board[7][0] = new Rook(BLACK);
        board[7][7] = new Rook(BLACK);
        //Knight
        board[0][1] = new Knight(WHITE);
        board[0][6] = new Knight(WHITE);
        board[7][1] = new Knight(BLACK);
        board[7][6] = new Knight(BLACK);
        //Bishop
        board[0][2] = new Bishop(WHITE);
        board[0][5] = new Bishop(WHITE);
        board[7][2] = new Bishop(BLACK);
        board[7][5] = new Bishop(BLACK);
        //Pawn
        for(int j=0 ; j<SIZE ; j++) {
            board[1][j] = new Pawn(WHITE);
            board[6][j] = new Pawn(BLACK);
        }


        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }

    }

    public void start() {
        while(true) {
            if(isCheck()) {
                System.out.println("Check!");
                if(isCheckMate()) {
                    System.out.println("Checkmate!");
                    System.out.println("Player " + otherPlayer(currentPlayer) + " wins!");
                }
            }
            if(isStaleMate()) {
                System.out.println("Stalemate!");
            }
            printBoard();
            promptMove();
        }
    }

    public void promptMove() {
        while(true) {
            System.out.printf("Player %d please move:\n", currentPlayer);
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String s = reader.readLine();
                StringTokenizer st = new StringTokenizer(s);
                boolean validMove = takeTurn(currentPlayer,
                        Integer.parseInt(st.nextToken()),
                        Integer.parseInt(st.nextToken()),
                        Integer.parseInt(st.nextToken()),
                        Integer.parseInt(st.nextToken()));
                if(validMove) {
                    break;
                }
            }catch (IOException e) {
                e.printStackTrace();
                break;
            }catch (Exception e) {
                System.out.println("Invalid Input, please input (i) (j) (newI) (newJ)");
            }
        }
    }

    public boolean takeTurn(int player, int i, int j, int newI, int newJ) {
        if(!inBound(i, j)) {
            System.out.println("i j out of bounds");
            return false;
        }
        if(!inBound(newI, newJ)) {
            System.out.println("newI newJ out of bounds");
            return false;
        }
        if(player != currentPlayer) {
            System.out.println("the other player's turn");
            return false;
        }
        if(board[i][j] == null) {
            System.out.println("i j is empty");
            return false;
        }
        Piece piece = board[i][j];
        if(piece.getPlayer() != player) {
            System.out.printf("%c is not your piece\n", piece.getChar());
            return false;
        }
        if(!piece.move(board, newI, newJ)) {
            System.out.printf("%c cannot move to %d %d\n", piece.getChar(), newI, newJ);
            return false;
        }
        if(currentPlayer == WHITE) {
            currentPlayer = BLACK;
        }else {
            currentPlayer = WHITE;
        }
        return true;
    }

    private King getKing(int player) {
        King king = null;
        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                if(board[i][j] != null && board[i][j] instanceof King && board[i][j].getPlayer() == player) {
                    king = (King)board[i][j];
                    break;
                }
            }
            if(king != null) {
                break;
            }
        }
        return king;
    }

    private boolean canKillPiece(Piece enemy) {
        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                if(board[i][j] != null && board[i][j].getPlayer() != enemy.getPlayer()) {
                    Piece piece = board[i][j];
                    if(piece.canMove(board, enemy.getI(), enemy.getJ())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isCheck() {
        Piece king = getKing(currentPlayer);
        if(king == null) {
            return false;
        }
        return canKillPiece(king);
    }

    public boolean isCheckMate() {
        Piece king = getKing(currentPlayer);
        if(king == null) {
            return false;
        }


        List<Piece> pieceListDefend = getPieces(currentPlayer);
        for(Piece piece : pieceListDefend) {
            for(int i=0 ; i<SIZE ; i++) {
                for(int j=0 ; j<SIZE ; j++) {
                    recordBoard();
                    if(piece.move(board, i, j)) {
                        if(!canKillPiece(king)) {
                            recoverBoard();
                            return false;
                        }
                    }
                    recoverBoard();
                }
            }
        }

        return true;
    }

    public boolean isStaleMate() {
        if(isCheck()) {
            return false;
        }
        Piece king = getKing(currentPlayer);
        if(king == null) {
            return false;
        }

        List<Piece> pieceListDefend = getPieces(currentPlayer);
        for(Piece piece : pieceListDefend) {
            for(int i=0 ; i<SIZE ; i++) {
                for(int j=0 ; j<SIZE ; j++) {
                    recordBoard();
                    if(piece.move(board, i, j)) {
                        if(!canKillPiece(king)) {
                            recoverBoard();
                            return false;
                        }
                    }
                    recoverBoard();
                }
            }
        }

        return true;
    }

    public List<Piece> getPieces(int player) {
        List<Piece> list = new ArrayList<>();
        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                if(board[i][j] != null && board[i][j].getPlayer() == player) {
                    list.add(board[i][j]);
                }
            }
        }
        return list;
    }

    public int otherPlayer(int player) {
        return player == WHITE ? BLACK : WHITE;
    }

    public void printBoard() {
        System.out.printf("Turn %d:\n", turn);
        for(int i=SIZE-1 ; i>=0 ; i--) {
            for(int j=0 ; j<SIZE ; j++) {
                if(board[i][j] == null) {
                    System.out.print('.');
                }else {
                    System.out.print(board[i][j].getChar());
                }
            }
            System.out.println();
        }

    }

    private void recordBoard() {
        tempBoard = new Piece[SIZE][SIZE];
        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                tempBoard[i][j] = board[i][j];
            }
        }
    }

    private boolean recoverBoard() {
        if(tempBoard == null) {
            return false;
        }
        for(int i=0 ; i<SIZE ; i++) {
            for(int j=0 ; j<SIZE ; j++) {
                board[i][j] = tempBoard[i][j];
                if(board[i][j] != null) {
                    board[i][j].setI(i);
                    board[i][j].setJ(j);
                }
            }
        }
        return true;
    }

    public static boolean inBound(int i, int j) {
        return i >= 0 && j >= 0 && i < Game.SIZE && j < Game.SIZE;
    }

    public Piece[][] getBoard() {
        return board;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

}
